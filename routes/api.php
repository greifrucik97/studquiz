<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;




Route::group([

    'middleware' => ['lang'],

], function ($router) {

    Route::post('auth/login', 'AuthController@login');//
    Route::post('auth/activeUser', 'UserController@activeUser');//
    Route::post('auth/rememberPassword', 'UserController@rememberPassword');//
    Route::post('auth/changePasswordWithCode', 'UserController@changePasswordWithCode');//
    Route::post('auth/createUserAccount', 'UserController@createUserAccount');//

});

Route::group([

    'middleware' => ['api', 'jwt.verify', 'lang'],
    'prefix' => 'auth'

], function ($router) {

    Route::post('logout', 'AuthController@logout');//
    Route::post('me', 'AuthController@me');//
    Route::post('refresh', 'AuthController@refresh');//
    Route::post('changePassword', 'UserController@changePassword');//

});

Route::group([

    'middleware' => ['api', 'jwt.verify', 'lang'],

], function ($router) {
    Route::post('test/startTest', 'TestController@startTest');
    Route::post('test/finishTest', 'TestController@finishTest');
    Route::post('test/getTestForUser', 'TestController@getTestForUser');
    Route::post('user/deleteUser', 'UserController@deleteUser');//
    Route::post('question/getTypes', 'QuestionTypeController@getTypes');//
    Route::post('question/createQuestion', 'QuestionController@createQuestion');//
    Route::post('question/deleteQuestion', 'QuestionController@deleteQuestion');
    Route::post('question/getQuestions', 'QuestionController@getQuestions');//
    Route::post('question/getQuestion', 'QuestionController@getQuestion');//
    Route::post('question/editQuestion', 'QuestionController@editQuestion');//
    Route::post('test/createTest', 'TestController@createTest');//
    Route::post('test/getTests', 'TestController@getTests');//
    Route::post('test/deleteTest', 'TestController@deleteTest');//
    Route::post('test/editTest', 'TestController@editTest');//
    Route::post('test/attachQuestion', 'TestController@attachQuestion');//
    Route::post('test/detachQuestion', 'TestController@detachQuestion');//
    Route::post('test/getTest', 'TestController@getTest');//
    Route::post('test/activeTest', 'TestController@activeTest');//
    Route::post('test/unActiveTest', 'TestController@unActiveTest');//
    Route::post('test/getUserTest', 'TestController@getUserTest');
    Route::post('test/doneQuestion', 'TestController@doneQuestion');
    Route::post('test/updateDate', 'TestController@updateDate');
    Route::post('test/attachUsersToTest', 'TestController@attachUsersToTest');
    Route::post('test/acceptTest', 'TestController@acceptTest');
    Route::post('test/getUserTests', 'TestController@getUserTests');
    Route::post('test/acceptInviteToTest', 'TestController@acceptInviteToTest');
    Route::post('tag/getTags', 'TagController@getTags');//
    Route::post('tag/detachTag', 'TagController@detachTag');//
    Route::post('tag/attachTag', 'TagController@attachTag');//
    Route::post('group/getUserGroups', 'GroupController@getUserGroups');//
    Route::post('group/getGroupUsers', 'GroupController@getGroupUsers');//
    Route::post('group/deleteGroup', 'GroupController@deleteGroup');//
    Route::post('group/addUserToGroup', 'GroupController@addUserToGroup');//
    Route::post('group/removeUserToGroup', 'GroupController@removeUserToGroup');//
    Route::post('image/uploadImages', 'ImageController@uploadImages');//
    Route::post('image/removeImage', 'ImageController@removeImage');
});