<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string type
 * @property mixed name
 */
class QuestionType extends Model
{
    protected $table = 'question_types';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = true;

    protected $fillable = ['type', 'name'];
}
