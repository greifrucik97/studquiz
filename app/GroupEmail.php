<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupEmail extends Model
{
    public function groups()
    {
        return $this->hasMany(Group::class,'group_id', 'id');
    }
}
