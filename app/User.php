<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property string firstName
 * @property string lastName
 * @property string indexNumber
 * @property string email
 * @property string role
 * @property string password
 * @property mixed group_id
 * @property mixed id
 * @property bool active
 * @property string activationLink
 * @method static find()
 * @method static save()
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            "user" => [
                "role" => $this->role,
                "id" => $this->id
            ]
        ];
    }

    public function setPasswordAttribute($password)
    {
        if ( !empty($password) ) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function rememberPassword()
    {
        return $this->hasOne(RememberPassword::class,'user_id', 'id');
    }

    public function tests()
    {
        return $this->hasMany(Test::class,'owner_id', 'id');
    }

    public function userTests()
    {
        return $this->hasMany(UserTest::class,'user_id', 'id');
    }



}
