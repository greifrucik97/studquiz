<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed|string code
 * @property int  user_id
 * @property string email
 */
class RememberPassword extends Model
{
    protected $table = 'remember_passwords';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = true;

    protected $fillable = ['code', 'user_id', 'email'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
