<?php

namespace App\Providers;

use App\Helpers\UserHelper;
use Illuminate\Support\Facades\Gate;
use Laravel\Horizon\Horizon;
use Laravel\Horizon\HorizonApplicationServiceProvider;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

    }

    protected function authorization()
    {
        Horizon::auth(function ($request) {
            return true;
        });
    }
}
