<?php

namespace App\Mail;

use App\Test;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteUserToTestMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Test
     */
    public $test;

    /**
     * Create a new message instance.
     *
     * @param Test $test
     */
    public function __construct(Test $test)
    {
        $this->test = $test;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.user.invite-user-to-test-mail');
    }
}
