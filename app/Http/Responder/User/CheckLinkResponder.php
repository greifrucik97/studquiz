<?php


namespace App\Http\Responder\User;


use App\AccountLinkUser;
use App\Http\Response\Responder;

class CheckLinkResponder implements Responder
{
    private $link;
    /**
     * CheckLinkResponder constructor.
     * @param $link
     */
    public function __construct($link)
    {
        $this->link = $link;
    }


    /**
     * @return array
     */
    public function generate(): array
    {
        return [AccountLinkUser::where('link', $this->link)->first()];
    }
}