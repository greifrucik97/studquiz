<?php


namespace App\Http\Response;


interface Responder
{

    /**
     * @return array
     */
    public function generate(): array;

}