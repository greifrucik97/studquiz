<?php


namespace App\Http\Services;


use App\Domain\Helpers\UserHelper;
use App\Http\Exceptions\DontHaveAccessException;
use App\Jobs\RegisterEmailJob;
use App\Jobs\SendRememberPasswordJob;
use App\Mail\SendRememberPasswordMail;
use App\RememberPassword;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserService
{
    /**
     * @var int
     */
    private $id;


    /**
     * UserService constructor.
     * @param int $user
     */
    public function __construct(?int $user)
    {
        if ($user != 0) {
            $this->id = $user;
        }
    }


    /**
     * @param int $user_id
     * @throws DontHaveAccessException
     */
    public function deleteUser(int $user_id): void
    {
        if ($user_id == UserHelper::getUserIdFromToken()) {

            $user = User::find($user_id);

            $user->delete();

        } else {
            throw new DontHaveAccessException();
        }

    }

    /**
     * @param string $firstName
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $password
     */
    public function createUser(string $firstName, string $lastName, string $email, string $password): void
    {
        $uniqueLink = false;

        $user = new User();
        $user->firstName = $firstName;
        $user->lastName = $lastName;

        $user->email = $email;
        $user->password = $password;

        $user->activationLink = Str::random(20);

        do  {
            $link = User::where('activationLink', $user->activationLink)->get();

            if($link->isEmpty()) {
                $uniqueLink = true;
            } else {
                $user->activationLink = Str::random(20);
            }

        }while (!$uniqueLink);


        $user->save();

        RegisterEmailJob::dispatch($user);
    }




    /**
     * @param string $email
     */
    public function sendCodeToRememberPassword(string $email): void
    {
        $unique = false;
        $user = User::where('email', $email)->first();

        $rememberPassword = RememberPassword::where('email', $email)->first();

        if (!$rememberPassword) {
            $rememberPassword = new RememberPassword();

            $rememberPassword->user_id = $user->id;
            $rememberPassword->email = $email;
            $rememberPassword->code = Str::random(10);


            do  {
                $rp = RememberPassword::where('code', $rememberPassword->code)->get();

                if($rp->isEmpty()) {
                    $unique = true;
                } else {
                    $rememberPassword->code = Str::random(10);
                }

            }while (!$unique);

            $rememberPassword->save();
        }

        SendRememberPasswordJob::dispatch($rememberPassword);
    }

    /**
     * @param string $password
     * @param string $codeString
     */
    public function changePasswordWithCode(string $password, string $codeString): void
    {
        $remember = RememberPassword::where('code', $codeString)->first();

        $user = $remember->user;
        $user->password = $password;
        $user->save();

        $remember->delete();
    }

    /**
     * @param string $oldPassword
     * @param string $password
     */
    public function changePassword(string $oldPassword, string $password): void
    {
        $user = User::find(UserHelper::getUserIdFromToken());

        $user->password = $password;
        $user->save();

    }

    /**
     * @param string $link
     */
    public function activeUser(string $link)
    {
        $user = User::where('activationLink', $link)->first();
        $user->activationLink = null;
        $user->active = true;
        $user->save();
    }

}