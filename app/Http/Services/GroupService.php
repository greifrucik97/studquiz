<?php


namespace App\Http\Services;


use App\Domain\Helpers\UserHelper;
use App\Group;
use App\GroupEmail;
use App\Http\Exceptions\DontHaveAccessException;
use App\User;

class GroupService
{

    public function createGroup(array $users, bool $createGroup, ?string $groupName)
    {
        if ($createGroup) {
            $group = new Group();
            $group->name = $groupName;
            $group->owner_id = UserHelper::getUserIdFromToken();
            $group->save();


            foreach ($users as $user) {
                $groupEmail = new GroupEmail();
                $groupEmail->email = $user;
                $groupEmail->group_id = $group->id;
                $groupEmail->save();
            }

        }
    }

    public function getUserGroups()
    {
        return Group::where('owner_id', UserHelper::getUserIdFromToken())->get()->toArray();
    }

    public function getGroupUsers(int $group_id)
    {
        $emails = [];
        $group = Group::find($group_id);
        $users = $group->users;
        foreach ($users as $user)
        {
            array_push($emails, $user->email);
        }
        return $emails;
    }

    public function deleteGroup(int $group_id) {

        $group = Group::find($group_id);

        if ($group->owner_id == UserHelper::getUserIdFromToken()) {

            $users = $group->users;

            foreach ($users as $user) {
                $user->delete();
            }

            $group->delete();

        } else {
            throw new DontHaveAccessException();
        }

    }

    public function addUserToGroup(int $group_id, string $email)
    {
        $group = Group::find($group_id);

        if ($group->owner_id == UserHelper::getUserIdFromToken()) {

            $groupEmail = new GroupEmail();
            $groupEmail->group_id = $group_id;
            $groupEmail->email = $email;

            $groupEmail->save();

        } else {
            throw new DontHaveAccessException();
        }
    }

    public function removeUserToGroup(int $group_id, string $email) {

        $group = Group::find($group_id);

        if ($group->owner_id == UserHelper::getUserIdFromToken()) {

            $groupEmail = GroupEmail::where('group_id', $group_id)->where('email', $email)->first();
            $groupEmail->delete();

        } else {
            throw new DontHaveAccessException();
        }
    }
}