<?php


namespace App\Http\Services;

use App\Domain\Helpers\UserHelper;
use App\Http\Exceptions\DontHaveAccessException;
use App\Jobs\InviteUserToTestJob;
use App\Question;
use App\Test;
use App\User;
use App\UserAnswer;
use App\UserTest;
use Carbon\Carbon;

class TestService
{

    /**
     * Funkcja tworzy test
     * @param string $name
     */
    public function createTest(string $name, bool $mixQuestions)
    {
        $test = new Test();
        $user = UserHelper::getUserFromToken();

        $test->name = $name;
        $test->owner_id = $user->id;
        $test->mixQuestions = $mixQuestions;

        $test->save();
    }

    /**
     * Funkcja zwraca testy użytkownika
     * @return array
     */
    public function getTests(): array
    {
        $user = UserHelper::getUserFromToken();

        return $user->tests()->get()->toArray();
    }

    /**
     * Funkcja usuwa test
     * @param int $test_id
     * @throws DontHaveAccessException
     */
    public function deleteTest(int $test_id)
    {
        $test = Test::find($test_id);
        if ($test->owner_id == UserHelper::getUserIdFromToken()) {
            $test->questions()->detach($test->questions);
            $userTests = UserTest::where('test_id', $test_id)->get();
            foreach ($userTests as $userTest)
            {
                $userTest->delete();
            }
            $test->delete();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funkcja zmienia nazwę testu
     * @param int $test_id
     * @param string $name
     * @throws DontHaveAccessException
     */
    public function editTest(int $test_id, string $name, bool $mixQuestions)
    {
        $test = Test::find($test_id);
        if ($test->owner_id == UserHelper::getUserIdFromToken()) {
            $test->name = $name;
            $test->mixQuestions = $mixQuestions;
            $test->save();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funkcja przypisuje pytanie do testu
     * @param int $test_id
     * @param int $question_id
     * @throws DontHaveAccessException
     */
    public function attachQuestion(int $test_id, int $question_id)
    {
        $test = Test::find($test_id);

        if ($test->owner_id == UserHelper::getUserIdFromToken()) {

            $ids = collect([$question_id]);
            $existing_ids = $test->questions()->whereIn('question_test.question_id', $ids)->pluck('question_test.question_id');
            $test->questions()->attach($ids->diff($existing_ids));
        } else {
            throw new DontHaveAccessException();
        }
    }


    /**
     * Funkcja zwraca test dla właściciela testu z dodatkami
     * @param int $test_id
     * @return mixed
     * @throws DontHaveAccessException
     */
    public function getTest(int $test_id)
    {
        $test = Test::where('id', $test_id)->with(['questions', 'owner', 'userTests','userTests.user', 'userTests.answers'])->first();

        if ($test->owner_id == UserHelper::getUserIdFromToken()) {
            return $test;
        } else {
            throw new DontHaveAccessException();
        }
    }



    /**
     * Funkcja usuwa pytanie z testu
     * @param int $test_id
     * @param int $question_id
     * @throws DontHaveAccessException
     */
    public function detachQuestion(int $test_id, int $question_id)
    {
        $test = Test::find($test_id);
        if ($test->owner_id == UserHelper::getUserIdFromToken()) {
            $question = Question::find($question_id);
            $test->questions()->detach($question);
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funckja aktywuje test do rozwiązania
     * @param int $test_id
     * @throws DontHaveAccessException
     */
    public function activeTest(int $test_id)
    {
        $test = Test::find($test_id);

        if ($test->owner_id == UserHelper::getUserIdFromToken()) {

            $test->active = 1;
            $test->save();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funckja zakańcza test
     * @param int $test_id
     * @throws DontHaveAccessException
     */
    public function unActiveTest(int $test_id)
    {
        $test = Test::find($test_id);

        if ($test->owner_id == UserHelper::getUserIdFromToken()) {
            $test->active = 2;
            $test->save();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funkcja startuje test dla użytkownika
     * @param int $test_id
     * @throws DontHaveAccessException
     */
    public function startTest(int $test_id)
    {
        $test = Test::find($test_id);
        $user_id = UserHelper::getUserIdFromToken();

        if ($test->active == 1) {
            $userTest = UserTest::where('user_id', $user_id)->where('test_id', $test_id)->first();

            $userTest->status = "started";
            $userTest->save();

            foreach ($test->questions as $question) {
                $userAnswer = new UserAnswer();
                $userAnswer->user_test_id = $userTest->id;
                $userAnswer->question_id = $question->id;
                $userAnswer->status = "created";
                $userAnswer->save();
            }

        }
    }

    /**
     * Funkcja zwraca wszystkie testy dla użytkownika
     * @return array
     */
    public function getUserTests() {
        $user = UserHelper::getUserFromToken();
        $tests = $user->userTests()->with(['test', 'test.owner'])->get()->toArray();
        foreach ($tests as $key => $item)
        {
            $pkt = 0.0;
            $questions = Test::find($item['test_id'])->questions;

            foreach ($questions as $question) {
                $pkt = $pkt + $question->pkt;
            }
            $tests[$key]['maxPkt'] = $pkt;
        }
        return $tests;
    }


    /**
     * Funkcja zwraca test do rozwiązania dla użytkownika
     * @param int $test_id
     * @return array
     * @throws DontHaveAccessException
     */
    public function getTestForUser(int $test_id) {
        $test = Test::where('id', $test_id)->with(['questions', 'owner', 'questions.type'])->first();
        $endDate = $test->endDate;
        $user_id = UserHelper::getUserIdFromToken();
        $userTest = UserTest::where('test_id', $test_id)->where('user_id', $user_id)->where('status', "started")->first();
        $questions = [];
        foreach ($test->questions as $que)
        {
            $q = [
                "answers" => $que->answers['answers'],
                "id" => $que->id,
                "pkt" => $que->pkt,
                "question" => $que->question,
                "type" => $que->type->type,
                "path" => $que->image_path
            ];
            array_push($questions, $q);
        }
        $result = [
            "id" => $test->id,
            "name" => $test->name,
            "owner" => [
                "firstName" => $test->owner->firstName,
                "lastName" => $test->owner->lastName
            ],
            "questions" => $questions,
            "endDate" => $endDate,
            'mixQuestions' => $test->mixQuestions
        ];
        if ($userTest) {
            return [
                "answers" => $userTest->answers->makeHidden(['created_at', 'pkt', 'status', 'updated_at']),
                "test" => $result
            ];
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funkcja służy do zakończenia testu przez użytkownika
     * @param $test_id
     * @param $answers
     */
    public function finishTest(int $test_id, array $answers)
    {
        $test = Test::find($test_id);

        $user = UserHelper::getUserFromToken();

        $userTest = UserTest::where('user_id', $user->id)->where('test_id', $test->id)->first();

        $userAnswers = $userTest->answers;

        if ($test->active != 1) {
            $userTest->inTime = false;
            $userTest->finishDate = Carbon::now();
            $userTest->status = "finished";
        } else {
            $userTest->inTime = true;
            $userTest->status = "finished";
            $userTest->finishDate = Carbon::now();
        }
        foreach ($answers as $answer) {
            $a = $userAnswers->where('question_id', $answer['question_id']);
            $item  = $a->first();
            $item->answer = $answer['answers'];
            $item->status = "done";
            $item->save();
        }

        $userTest->save();

        $this->checkTest($userTest);
    }

    /**
     * Funkcja sprawdza poprawność odpowiedzi
     * @param UserTest $userTest
     */
    private function checkTest(UserTest $userTest) {
        $userAnswers = $userTest->answers;
        foreach ($userAnswers as $userAnswer) {
            $question = Question::where('id', $userAnswer->question_id)->first();
            $type = $question->type->type;
            if ($type === "singleChoice") {
                $isGood = false;
                $questionAnswer = $question->answers['goodAnswers'];
                $answer = $userAnswer->answer;

                $isGood = in_array($answer, $questionAnswer);

                if ($isGood) {
                    $userAnswer->pkt = $question->pkt;
                    $userAnswer->status = "checked";
                    $userAnswer->save();
                } else {
                    $userAnswer->pkt = 0.0;
                    $userAnswer->status = "checked";
                    $userAnswer->save();
                }
            } else if ($type === "multipleChoice") {
                $questionAnswer = collect($question->answers['goodAnswers']);
                $answer = collect($userAnswer->answer);

                $diff = $questionAnswer->diff($answer);
                $diff2 = $answer->diff($questionAnswer);

                if ($diff->isEmpty() && $diff2->isEmpty()) {
                    $userAnswer->pkt = $question->pkt;
                    $userAnswer->status = "checked";
                    $userAnswer->save();
                } else {
                    $userAnswer->pkt = 0.0;
                    $userAnswer->status = "checked";
                    $userAnswer->save();
                }
            }
        }
    }



    /**
     * Funkcja dla właściela sprawia ze pytanie jest sprawdzone oraz nadane są jej punkty
     * @param int $id
     * @param float $pkt
     * @throws DontHaveAccessException
     */
    public function doneQuestion(int $id, float $pkt) {
        $userAnswer = UserAnswer::find($id);

        $userTest = UserTest::find($userAnswer->user_test_id);

        $owner = $userTest->test->owner;

        if ( $owner->id === UserHelper::getUserIdFromToken() ) {
            $userAnswer->pkt = $pkt;
            $userAnswer->status = "checked";

            $userAnswer->save();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Akcja dla właściela sprawia, że test jest sprawdzony
     * @param int $id
     * @throws DontHaveAccessException
     */
    public function acceptTest(int $id) {
        $userTest = UserTest::find($id);

        $owner = $userTest->test->owner;

        if ( $owner->id === UserHelper::getUserIdFromToken() ) {

            $pkt = 0.0;
            foreach ($userTest->answers as $answer) {
                $pkt = $pkt + $answer->pkt;
            }
            $userTest->status = "checked";
            $userTest->pkt = $pkt;
            $userTest->save();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funkcja zaprasza użytkownika do testu
     * @param array $users
     * @param int $test_id
     * @return array
     */
    public function attachUsersToTest(array $users, int $test_id)
    {
        $findedUsers = [];
        $dontFindedUsers = [];

        $test = Test::find($test_id);

        foreach ($users as $user)
        {
            $u = User::where('email', $user)->first();
            if ($u) {
                $ut = UserTest::where('user_id', $u->id)->where('test_id', $test_id)->first();

                if (!$ut) {

                    array_push($findedUsers, $user);
                    $userTest = new UserTest();

                    $userTest->user_id = $u->id;
                    $userTest->test_id = $test_id;
                    $userTest->status = "invited";

                    $userTest->save();

                    InviteUserToTestJob::dispatch($u, $test);
                }
            } else {
                array_push($dontFindedUsers, $user);
            }
        }

        return [
            'dontFoundUsers' => $dontFindedUsers,
            'foundUsers' => $findedUsers
        ];


    }

    /**
     * Funkcja zmienia daty
     * @param int $test_id
     * @param string $type
     * @param string $date
     * @throws DontHaveAccessException
     */
    public function updateDate(int $test_id, string $type, string $date)
    {
        $date = Carbon::createFromTimeString($date);

        $test = Test::find($test_id);

        if ($test->owner_id === UserHelper::getUserIdFromToken()) {
            if ($type === "startDate") {
                $test->startDate = $date;
            } else {
                $test->endDate = $date;
            }
            $test->save();
        } else {
            throw new DontHaveAccessException();
        }
    }

    /**
     * Funkcja zwraca dane do widoku sprawdzenia testu
     * @param int $id
     * @return mixed
     */
    public function getUserTest(int $id) {

        //TODO check role test
        $test = UserTest::where('id', $id)->with(['answers', 'user', 'test', 'test.owner', 'test.questions', 'test.questions.type'])->get()->toArray();

        return $test;
    }

    /**
     * Funkcja akceptuje zaproszenie do testu
     * @param int $test_id
     */
    public function acceptInviteToTest(int $test_id) {
        $user = UserHelper::getUserFromToken();
        $userTest = UserTest::where('user_id', $user->id)->where('test_id', $test_id)->first();
        $userTest->status = 'accepted';
        $userTest->save();
    }

}