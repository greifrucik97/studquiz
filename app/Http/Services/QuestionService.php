<?php


namespace App\Http\Services;


use App\Domain\Helpers\UserHelper;
use App\Http\Exceptions\DontHaveAccessException;
use App\Question;
use App\QuestionType;
use App\User;
use App\UserAnswer;
use Barryvdh\DomPDF\PDF;

class QuestionService
{

    public function createQuestion(string $q, string $type, float $pkt, array $answers, array $correctAnswer, array $tags, ?array $assignImages): void
    {
        $title = collect($assignImages)->filter( function ($value){
            if (array_key_exists('name', $value)) {
                return $value['name'] === "title";
            }
            return null;
        })->first();

        if ($title !== null) {
            $title = $title['value'];
        }

        $question = new Question();
        $question->owner_id = UserHelper::getUserIdFromToken();
        $t = QuestionType::where('type', $type)->first();
        $question->type_id = $t->id;
        $question->question = $q;
        $question->pkt = $pkt;
        $question->answers = $this->createAnswers($t->id, $answers, $correctAnswer, $assignImages);

        if ($title) {
            $question->image_path = $title;
        }


        $tagService = new TagService();

        $question->save();

        foreach ($tags as $tag) {
            $newTag = $tagService->createTag($tag);
            $question->tags()->attach($newTag);
        }
    }

    public function createAnswers(int $type_id, array $answers, array $correctAnswer, array $assignImages) {

        $questionType = QuestionType::find($type_id);

        switch ($questionType->type){
            case "singleChoice":
            case "multipleChoice":
                if (count($assignImages) > 0) {

                    $ans = [];

                    foreach ($answers as $answer) {
                        $name = collect($assignImages)->where('name', $answer['id'])->first();

                        if ($name !== null) {
                            $name = $name['value'];
                        }

                        if ($name) {
                            $a = [
                                'id' => $answer['id'],
                                'name' => $answer['name'],
                                'path' => $name
                            ];
                        } else {
                            $a = [
                                'id' => $answer['id'],
                                'name' => $answer['name'],
                            ];
                        }
                        array_push($ans, $a);
                    }
                    return [
                        'answers' => $ans,
                        'goodAnswers' => $correctAnswer
                    ];
                } else {
                    return [
                        'answers' => $answers,
                        'goodAnswers' => $correctAnswer
                    ];
                }
            break;
            case "shortOpenQuestion":
            case "longOpenQuestion":
                return ["answers" => $correctAnswer[0]];
            default:
                return [];
        }

    }

    public function editAnswer(int $type_id, array $answers, array $correctAnswer, array $assignImages) {

        $questionType = QuestionType::find($type_id);

        switch ($questionType->type){
            case "singleChoice":
            case "multipleChoice":
                    $ans = [];
                    foreach ($answers as $answer) {

                        $name = collect($assignImages)->where('name', $answer['id'])->first();

                        if ($name !== null) {
                            $name = $name['value'];
                        }

                        if ($name) {
                            $a = [
                                'id' => $answer['id'],
                                'name' => $answer['name'],
                                'path' => $name
                            ];
                        } else {
                                $a = [
                                    'id' => $answer['id'],
                                    'name' => $answer['name'],
                                ];
                        }
                        array_push($ans, $a);
                    }
                    return [
                        'answers' => $ans,
                        'goodAnswers' => $correctAnswer
                    ];
                break;
            case "shortOpenQuestion":
            case "longOpenQuestion":
                return ["answers" => $correctAnswer[0]];
            default:
                return [];
        }

    }

    /**
     * @param int $id
     * @throws DontHaveAccessException
     */
    public function deleteQuestion(int $id) {

        $question = Question::find($id);
        if ($question->owner_id == UserHelper::getUserIdFromToken()){
            $tests = $question->tests;

            foreach ($tests as $t) {
                $t->questions()->detach($question);
            }

            $question->tags()->detach($question->tags);

            $userAnswers = UserAnswer::where('question_id', $id)->get();

            foreach ($userAnswers as $userAnswer) {
                $userAnswer->delete();
            }

            $question->delete();
        } else {
            throw new DontHaveAccessException();
        }
    }

    public function getQuestions(string $q, array $filters) {


        $query = Question::select('*');

        if ($q !== "*") {
            $searchWildcard = '%' . $q . '%';
            $query->where('question', 'LIKE', $searchWildcard)->where('owner_id', UserHelper::getUserIdFromToken());
        } else {
            $query->where('owner_id', UserHelper::getUserIdFromToken());
        }



        foreach($filters as $key => $value){

            if ($key === 'tags_id' && count($filters['tags_id']) > 0) {
                $query->whereHas('tags', function($q) use($value) {
                    $q->whereIn('tag_id', $value);
                });
            } else {
                if(!empty($filters[$key])){
                    $query->whereIn($key, $value);
                }
            }
        }

        $data = $query->with(['owner', 'type', 'tags'])->paginate(10);

        return [
            "data" => $data->items(),
            "page" => $data->currentPage(),
            "last_page" => $data->lastPage(),
            "on_page" => $data->count(),
            "total" => $data->total(),
            "filters" => $filters
        ];

    }

    public function getQuestion(int $id) {

        return Question::where("id", $id)->with(['type', 'tags'])->first();

    }

    /**
     * @param int $question_id
     * @param string $q
     * @param int $pkt
     * @param array $answers
     * @param array $correctAnswer
     * @param array $tags
     * @param array $assignImages
     * @param array $imagesReturn
     * @throws DontHaveAccessException
     */
    public function editQuestion(int $question_id, string $q, int $pkt, array $answers, array $correctAnswer, array $tags, ?array $assignImages)
    {

        $title = collect($assignImages)->filter( function ($value){
            if (array_key_exists('name', $value)) {
                return $value['name'] === "title";
            }
            return null;
        })->first();

        if ($title !== null) {
            $title = $title['value'];
        }

        $question = Question::find($question_id);

        if ($question->owner_id == UserHelper::getUserIdFromToken()) {
            $question->question = $q;
            $question->pkt = $pkt;
            $question->image_path = null;
            if ($title) {
                $question->image_path = $title;
            }
            $question->tags()->detach($question->tags);

            $tagService = new TagService();

            foreach ($tags as $tag) {
                $newTag = $tagService->createTag($tag);
                $question->tags()->attach($newTag);
            }

            $question->answers = $this->editAnswer($question->type_id, $answers, $correctAnswer, $assignImages, $question->answers);

            $question->save();
        } else {
            throw new DontHaveAccessException();
        }

    }

}