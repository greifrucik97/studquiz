<?php

namespace App\Http\Services;

use App\Domain\Helpers\UserHelper;
use App\Http\Exceptions\DontHaveAccessException;
use App\Question;
use App\Tag;

class TagService
{
    public function createTag(string $name)
    {
        $tag = Tag::where('name', $name)->first();

        if ($tag) {
            return $tag;
        } else {
            $tag = new Tag();
            $tag->name = $name;
            $tag->owner_id = UserHelper::getUserIdFromToken();
            $tag->save();
            return $tag;
        }
    }

    public function getTags()
    {
        return Tag::where('owner_id', UserHelper::getUserIdFromToken())->get()->toArray();
    }

    /**
     * @param int $question_id
     * @param string $name
     * @throws DontHaveAccessException
     */
    public function attachTag(int $question_id, string $name){

        $tag = $this->createTag($name);

        $question = Question::find($question_id);

        $xd = $question->owner_id;
        $xd1 = UserHelper::getUserIdFromToken();

        if ($question->owner_id == UserHelper::getUserIdFromToken()) {
            $question->tags()->attach($tag);
        } else {
            throw new DontHaveAccessException();
        }

    }

    /**
     * @param int $question_id
     * @param int $tag_id
     * @throws DontHaveAccessException
     */
    public function detachTag(int $question_id, int $tag_id){

        $tag = Tag::find($tag_id);

        $question = Question::find($question_id);

        if ($question->owner_id == UserHelper::getUserIdFromToken()) {
            $question->tags()->detach($tag);
        } else {
            throw new DontHaveAccessException();
        }
    }

}