<?php


namespace App\Http\Services;


use App\QuestionType;

class QuestionTypeService
{

    public function getTypes() {

        return [
            QuestionType::all()
        ];

    }

}