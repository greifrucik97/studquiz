<?php

namespace App\Http\Controllers;

use App\Http\Exceptions\DontHaveAccessException;
use App\Http\Request\Tag\AttachTagModel;
use App\Http\Request\Tag\DetachTagModel;
use App\Http\Response\APIResponse;
use App\Http\Services\TagService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TagController extends Controller
{

    public $service;

    /**
     * TagController constructor.
     */
    public function __construct()
    {
        $this->service = new TagService();
    }

    public function getTags(Request $request): JsonResponse
    {
        return APIResponse::produceResponse(200, "", $this->service->getTags());
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function detachTag(Request $request): JsonResponse
    {
        $model = new DetachTagModel($request);
        $model->validate();

        $this->service->detachTag($model->question_id, $model->tag_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function attachTag(Request $request): JsonResponse
    {
        $model = new AttachTagModel($request);
        $model->validate();

        $this->service->attachTag($model->question_id, $model->name);

        return APIResponse::produceResponse(200);
    }

}
