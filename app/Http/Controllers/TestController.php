<?php

namespace App\Http\Controllers;

use App\Http\Exceptions\DontHaveAccessException;
use App\Http\Request\Test\AttachQuestionModel;
use App\Http\Request\Test\AttachUsersToTestModel;
use App\Http\Request\Test\CreateTestModel;
use App\Http\Request\Test\DoneQuestionModel;
use App\Http\Request\Test\EditTestModel;
use App\Http\Request\Test\FinishTestModel;
use App\Http\Request\Test\GetUserTest;
use App\Http\Request\Test\TestIDModel;
use App\Http\Request\Test\UpdateDateModel;
use App\Http\Response\APIResponse;
use App\Http\Services\GroupService;
use App\Http\Services\TestService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Psy\Util\Json;

class TestController extends Controller
{
    public $service;

    /**
     * TestController constructor.
     */
    public function __construct()
    {
        $this->service = new TestService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createTest(Request $request): JsonResponse
    {
        $model = new CreateTestModel($request);
        $model->validate();

        $this->service->createTest($model->name, $request->get('mixQuestions'));

        return APIResponse::produceResponse(200);
    }

    /**
     * @return JsonResponse
     */
    public function getTests()
    {
        return APIResponse::produceResponse(200, "", $this->service->getTests());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function deleteTest(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        $this->service->deleteTest($model->test_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function editTest(Request $request): JsonResponse
    {
        $model = new EditTestModel($request);
        $model->validate();

        $this->service->editTest($model->test_id, $model->name, $request->get('mixQuestions'));

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function attachQuestion(Request $request): JsonResponse
    {
        $model = new AttachQuestionModel($request);
        $model->validate();

        $this->service->attachQuestion($model->test_id, $model->question_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function detachQuestion(Request $request): JsonResponse
    {
        $model = new AttachQuestionModel($request);
        $model->validate();

        $this->service->detachQuestion($model->test_id, $model->question_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function getTest(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        return APIResponse::produceResponse(200, "", [$this->service->getTest($model->test_id)]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function activeTest(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        $this->service->activeTest($model->test_id);

        return APIResponse::produceResponse(200);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function unActiveTest(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        $this->service->unActiveTest($model->test_id);

        return APIResponse::produceResponse(200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function startTest(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        $this->service->startTest($model->test_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function getTestForUser(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        return APIResponse::produceResponse(200, "", $this->service->getTestForUser($model->test_id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function finishTest(Request $request): JsonResponse
    {
        $model = new FinishTestModel($request);
        $model->validate();

        $this->service->finishTest($model->test_id, $model->answers);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getUserTest(Request $request): JsonResponse
    {
        $model = new GetUserTest($request);
        $model->validate();

        return APIResponse::produceResponse(200, "", $this->service->getUserTest($model->user_test_id));
    }

    /**
     * @return JsonResponse
     */
    public function getUserTests(): JsonResponse
    {
        return APIResponse::produceResponse(200, "", $this->service->getUserTests());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function doneQuestion(Request $request): JsonResponse
    {
        $model = new DoneQuestionModel($request);
        $model->validate();

        $this->service->doneQuestion($model->user_answer_id, $model->pkt);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function acceptTest(Request $request): JsonResponse
    {
        $model = new GetUserTest($request);
        $model->validate();

        $this->service->acceptTest($model->user_test_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function attachUsersToTest(Request $request) {

        $model = new  AttachUsersToTestModel($request);
        $model->validate();

        $groupService = new GroupService();
        $groupService->createGroup($model->users, $model->createGroup, $model->groupName);

        return APIResponse::produceResponse(200, "", $this->service->attachUsersToTest($model->users, $model->test_id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DontHaveAccessException
     */
    public function updateDate(Request $request)
    {
        $model = new  UpdateDateModel($request);
        $model->validate();

        $this->service->updateDate($model->test_id, $model->type, $model->date);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function acceptInviteToTest(Request $request): JsonResponse
    {
        $model = new TestIDModel($request);
        $model->validate();

        $this->service->acceptInviteToTest($model->test_id);

        return APIResponse::produceResponse(200);
    }

}
