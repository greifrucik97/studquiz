<?php

namespace App\Http\Controllers;

use App\Http\Request\Question\RemoveImageModel;
use App\Http\Response\APIResponse;
use App\Image;
use App\Question;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageController extends Controller
{

    public function uploadImages(Request $request) {
        $images = [];
        if($request->hasFile('images')) {
            $files = $request->images;
            foreach($files as $file) {
                $uniqueName = time() . Str::random(5). "." . $file->getClientOriginalExtension();
                $path = Storage::putFileAs('images', new File($file->path()), $uniqueName);
                $image = new Image();
                $image->name = $file->getClientOriginalName();
                $image->path = Storage::url($path);
                $image->save();
                array_push($images, $image);
            }
        }
        return APIResponse::produceResponse(200, "", $images);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function removeImage(Request $request) {

        $model = new RemoveImageModel($request);
        $model->validate();

        $question = Question::find($model->question_id);
        if ($model->type === 'title') {
            $question->image_path = "";
        }

        if ($model->type === 'answer') {

            $answers = $question->answers;
            foreach ($answers['answers'] as $key => $answer) {
                if ($answer['id'] == $model->answer_id) {
                    $answers['answers'][$key]['path'] = "";
                }
            }
            $question->answers = $answers;
        }

        $question->save();


        return APIResponse::produceResponse(200);

    }

}
