<?php

namespace App\Http\Controllers;

use App\Http\Exceptions\DontHaveAccessException;
use App\Http\Request\Question\CreateQuestionModel;
use App\Http\Request\Question\EditQuestionModel;
use App\Http\Request\Question\QuestionIDModel;
use App\Http\Response\APIResponse;
use App\Http\Services\QuestionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class QuestionController extends Controller
{

    /**
     * @var QuestionService
     */
    public $service;

    /**
     * QuestionController constructor.
     */
    public function __construct()
    {
        $this->service = new QuestionService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createQuestion(Request $request): JsonResponse
    {

        $model = new CreateQuestionModel($request);
        $model->validate();

        $assignImages = $request->get('assignImages') ?? [];

        if ($assignImages == null) {
            $assignImages = [];
        }

        $this->service->createQuestion(
            $model->question,
            $model->type,
            $model->pkt,
            $model->answers,
            $model->correctAnswer,
            $model->tags,
            $assignImages
        );

        return APIResponse::produceResponse(200);

    }

    public function getQuestions(Request $request): JsonResponse
    {
        $requestQ = $request->get('q') ?? "*";
        $filter = $request->get('filter') ?? [];
        return APIResponse::produceResponse(200, "", $this->service->getQuestions($requestQ, $filter));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getQuestion(Request $request): JsonResponse
    {
        $model = new QuestionIDModel($request);
        $model->validate();

        return APIResponse::produceResponse(200, "", [$this->service->getQuestion($model->question_id)]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DontHaveAccessException
     */
    public function deleteQuestion(Request $request): JsonResponse
    {
        $model = new QuestionIDModel($request);
        $model->validate();

        $this->service->deleteQuestion($model->question_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws DontHaveAccessException
     * @throws ValidationException
     */
    public function editQuestion(Request $request): JsonResponse
    {
        $model = new EditQuestionModel($request);
        $model->validate();

        $assignImages = $request->get('assignImages') ?? [];

        $this->service->editQuestion(
            $model->question_id,
            $model->question,
            $model->pkt,
            $model->answers,
            $model->correctAnswer,
            $model->tags,
            $assignImages);

        return APIResponse::produceResponse(200);
    }

}
