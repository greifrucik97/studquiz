<?php

namespace App\Http\Controllers;

use App\Domain\Helpers\ReturnStatus;
use App\Http\Exceptions\DontHaveAccessException;
use App\Http\Request\User\ChangePasswordModel;
use App\Http\Request\User\ChangePasswordWithCodeModel;
use App\Http\Request\User\ActiveUserModel;
use App\Http\Request\User\CreateUserAccountModel;
use App\Http\Request\User\RememberPasswordModel;
use App\Http\Request\User\UserIDModel;
use App\Http\Request\User\UserLessonModel;
use App\Http\Responder\User\CheckLinkResponder;
use App\Http\Response\APIResponse;
use App\Http\Services\UserService;
use App\Jobs\RegisterEmailJob;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    /**
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createUserAccount(Request $request): JsonResponse
    {
        $model = new CreateUserAccountModel($request);
        $model->validate();

        $userService = new UserService(0);

        $userService->createUser(
            $model->firstName,
            $model->lastName,
            $model->email,
            $model->password
        );

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function activeUser(Request $request): JsonResponse
    {
        $model = new ActiveUserModel($request);
        $model->validate();

        $userService = new UserService(0);

        $userService->activeUser($model->link);
        return APIResponse::produceResponse(200);
    }



    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DontHaveAccessException
     */
    public function deleteUser(Request $request):JsonResponse
    {
        $model = new UserIDModel($request);
        $model->validate();

        $userService = new UserService(0);
        $userService->deleteUser($model->user_id);


        return APIResponse::produceResponse(200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function rememberPassword(Request $request) :JsonResponse
    {
        $model = new RememberPasswordModel($request);
        $model->validate();

        $userService = new UserService(0);
        $userService->sendCodeToRememberPassword($model->email);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changePasswordWithCode(Request $request): JsonResponse
    {

        $model = new ChangePasswordWithCodeModel($request);
        $model->validate();

        $userService = new UserService(0);
        $userService->changePasswordWithCode($model->password, $model->code);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changePassword(Request $request): JsonResponse
    {
        $model = new ChangePasswordModel($request);
        $model->validate();

        $userService = new UserService(0);
        $userService->changePassword($model->oldPassword, $model->password);

        return APIResponse::produceResponse(200);
    }


}
