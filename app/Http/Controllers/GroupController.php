<?php


namespace App\Http\Controllers;


use App\Http\Exceptions\DontHaveAccessException;
use App\Http\Request\Group\GroupIDEmailModel;
use App\Http\Request\Group\GroupIDModel;
use App\Http\Response\APIResponse;
use App\Http\Services\GroupService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class GroupController  extends Controller
{
    /**
     * @var GroupService
     */
    public $service;

    /**
     * GroupController constructor.
     */
    public function __construct()
    {
        $this->service = new GroupService();
    }

    public function getUserGroups(): JsonResponse
    {
        return APIResponse::produceResponse(200,"", $this->service->getUserGroups());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getGroupUsers(Request $request): JsonResponse
    {
        $model = new GroupIDModel($request);
        $model->validate();

        return APIResponse::produceResponse(200, "", $this->service->getGroupUsers($model->group_id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DontHaveAccessException
     */
    public function deleteGroup(Request $request): JsonResponse
    {
        $model = new GroupIDModel($request);
        $model->validate();

        $this->service->deleteGroup($model->group_id);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DontHaveAccessException
     */
    public function addUserToGroup(Request $request): JsonResponse
    {
        $model = new GroupIDEmailModel($request);
        $model->validate();

        $this->service->addUserToGroup($model->group_id, $model->email);

        return APIResponse::produceResponse(200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws DontHaveAccessException
     */
    public function removeUserToGroup(Request $request): JsonResponse
    {
        $model = new GroupIDEmailModel($request);
        $model->validate();

        $this->service->removeUserToGroup($model->group_id, $model->email);

        return APIResponse::produceResponse(200);
    }




}
