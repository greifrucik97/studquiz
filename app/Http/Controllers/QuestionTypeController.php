<?php

namespace App\Http\Controllers;

use App\Http\Response\APIResponse;
use App\Http\Services\QuestionTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class QuestionTypeController extends Controller
{

    /**
     * @var QuestionTypeService
     */
    public $service;

    /**
     * QuestionTypeController constructor.
     */
    public function __construct()
    {
        $this->service = new QuestionTypeService();
    }

    /**
     * @return JsonResponse
     */
    public function getTypes(): JsonResponse
    {
        return APIResponse::produceResponse(200, '', $this->service->getTypes());
    }
}
