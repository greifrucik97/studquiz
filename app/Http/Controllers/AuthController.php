<?php

namespace App\Http\Controllers;

use App\Domain\Helpers\ReturnStatus;
use App\Exceptions\NoActiveAccount;
use App\Jobs\SendWelcomeMailQueue;
use App\Mail\WelcomeUserMail;
use App\User;
use Illuminate\Contracts\Queue\Job;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function vue()
    {
        return view('welcome');
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return JsonResponse
     * @throws NoActiveAccount
     */
    public function login()
    {

        $credentials = [
            'email' => \request('email'),
            'password' => \request('password'),
        ];

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 400);
        }


        if (!\auth()->user()->active) {
            throw new NoActiveAccount();
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
       return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }


}
