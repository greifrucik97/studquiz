<?php


namespace App\Http\Request\Question;


use App\Http\Request\RequestParametersModel;

class EditQuestionModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::QUESTION_ID_PARAM,
            self::QUESTION_PARAM,
            self::PKT_PARAM,
            self::ANSWERS_PARAM,
            self::CORRECT_ANSWER_PARAM,
            self::TAGS_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::QUESTION_ID_PARAM => self::QUESTION_ID_RULE,
            self::QUESTION_PARAM => self::QUESTION_RULE,
            self::PKT_PARAM => self::PKT_RULE,
            self::ANSWERS_PARAM => self::ANSWERS_RULE,
            self::CORRECT_ANSWER_PARAM => self::CORRECT_ANSWER_RULE,
            self::TAGS_PARAM => self::TAGS_RULE
        ];
    }


}