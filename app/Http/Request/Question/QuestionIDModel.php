<?php


namespace App\Http\Request\Question;


use App\Http\Request\RequestParametersModel;

class QuestionIDModel extends RequestParametersModel
{


    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::QUESTION_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::QUESTION_ID_PARAM => self::QUESTION_ID_RULE
        ];
    }


}