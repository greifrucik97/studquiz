<?php


namespace App\Http\Request\Question;


use App\Http\Request\RequestParametersModel;

class RemoveImageModel extends RequestParametersModel
{

    protected function parameterKeys(): array
    {
        return [
            'type',
            'question_id',
            'answer_id'
        ];
    }

    protected function rules(): array
    {
        return [];
    }


}