<?php


namespace App\Http\Request\Question;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed question
 * @property mixed type_id
 * @property mixed pkt
 * @property mixed answers
 * @property mixed correctAnswer
 * @property mixed type
 * @property mixed tags
 */
class CreateQuestionModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::QUESTION_PARAM,
            self::TYPE_PARAM,
            self::PKT_PARAM,
            self::ANSWERS_PARAM,
            self::CORRECT_ANSWER_PARAM,
            self::TAGS_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::QUESTION_PARAM => self::QUESTION_RULE,
            self::TYPE_PARAM => self::TYPE_RULE,
            self::PKT_PARAM => self::PKT_RULE,
            self::ANSWERS_PARAM => self::ANSWERS_RULE,
            self::CORRECT_ANSWER_PARAM => self::CORRECT_ANSWER_RULE,
            self::TAGS_PARAM => self::TAGS_RULE
        ];
    }


}