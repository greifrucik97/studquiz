<?php


namespace App\Http\Request\Group;


use App\Http\Request\RequestParametersModel;

class GroupIDModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::GROUP_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::GROUP_ID_PARAM => self::GROUP_ID_RULE
        ];
    }


}