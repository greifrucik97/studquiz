<?php

namespace App\Http\Request\Group;


use App\Http\Request\RequestParametersModel;

class GroupIDEmailModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::GROUP_ID_PARAM,
            self::EMAIL_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::GROUP_ID_PARAM => self::GROUP_ID_RULE,
            self::EMAIL_PARAM => self::EMAIL_GROUP_RULE
        ];
    }


}
