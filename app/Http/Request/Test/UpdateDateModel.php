<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class UpdateDateModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::TEST_ID_PARAM,
            self::TYPE_PARAM,
            self::DATE_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::TEST_ID_PARAM => self::TEST_ID_RULE,
            self::TYPE_PARAM => self::TYPE2_RULE,
            self::DATE_PARAM => self::DATE_RULE
        ];
    }


}