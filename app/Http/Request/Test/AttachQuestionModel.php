<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class AttachQuestionModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::TEST_ID_PARAM,
            self::QUESTION_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::TEST_ID_PARAM => self::TEST_ID_RULE,
            self::QUESTION_ID_PARAM => self::QUESTION_ID_RULE
        ];
    }


}