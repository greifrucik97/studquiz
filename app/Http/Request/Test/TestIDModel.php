<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class TestIDModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::TEST_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::TEST_ID_PARAM => self::TEST_ID_RULE
        ];
    }


}