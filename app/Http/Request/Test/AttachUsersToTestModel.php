<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class AttachUsersToTestModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::USERS_PARAM,
            self::TEST_ID_PARAM,
            self::GROUP_NAME_PARAM,
            self::CREATE_GROUP_PARAM
        ];
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [
            self::USERS_PARAM => self::USERS_RULE,
            self::TEST_ID_PARAM => self::TEST_ID_RULE,
            self::GROUP_NAME_PARAM => self::GROUP_NAME_RULE,
            self::CREATE_GROUP_PARAM => self::CREATE_GROUP_RULE
        ];
    }


}