<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class EditTestModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::TEST_ID_PARAM,
            self::NAME_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::TEST_ID_PARAM => self::TEST_ID_RULE,
            self::NAME_PARAM => self::NAME_RULE
        ];
    }


}