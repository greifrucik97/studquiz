<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class FinishTestModel extends RequestParametersModel
{


    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::TEST_ID_PARAM,
            self::ANSWERS_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::TEST_ID_PARAM => self::TEST_ID_RULE,
            self::ANSWERS_PARAM => self::ANSWERS_RULE
        ];
    }


}