<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class CreateTestModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::NAME_PARAM,
        ];
    }

    protected function rules(): array
    {
        return [
            self::NAME_PARAM => self::NAME_RULE,
        ];
    }


}