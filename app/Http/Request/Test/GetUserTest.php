<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class GetUserTest extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::USER_TEST_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::USER_TEST_ID_PARAM => self::USER_TEST_ID_RULE
        ];
    }


}