<?php


namespace App\Http\Request\Test;


use App\Http\Request\RequestParametersModel;

class DoneQuestionModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::PKT_PARAM,
            self::USER_ANSWER_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::PKT_PARAM => self::PKT_RULE,
            self::USER_ANSWER_ID_PARAM => self::USER_ANSWER_ID_RULE
        ];
    }


}