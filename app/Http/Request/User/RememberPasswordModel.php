<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed email
 */
class RememberPasswordModel extends RequestParametersModel
{


    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
       return [
           self::EMAIL_PARAM
       ];
    }

    protected function rules(): array
    {
        return [
            self::EMAIL_PARAM => self::EMAIL_REMEMBER_PASSWORD_RULE
        ];
    }


}