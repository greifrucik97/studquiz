<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed firstName
 * @property mixed lastName
 * @property mixed indexNumber
 * @property mixed email
 * @property mixed password
 * @property mixed role
 * @property mixed link
 */
class CreateUserAccountModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::FIRSTNAME_PARAM,
            self::LASTNAME_PARAM,
            self::EMAIL_PARAM,
            self::PASSWORD_PARAM,
            self::PASSWORDCONFIRM_PARAM,
        ];
    }

    protected function rules(): array
    {
        return [
            self::FIRSTNAME_PARAM => self::FIRSTNAME_RULE,
            self::LASTNAME_PARAM => self::LASTNAME_RULE,
            self::EMAIL_PARAM => self::EMAIL_RULE,
            self::PASSWORD_PARAM => self::PASSWORD_RULE,
            self::PASSWORDCONFIRM_PARAM => self::PASSWORDCONFIRM_RULE,
        ];
    }

    protected function messages(): array
    {
        return [
            'email.unique' => __('Instnieje już taki użytkownik w bazie danych')
        ];
    }


}