<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed passwordConfirm
 * @property mixed password
 * @property mixed oldPassword
 */
class ChangePasswordModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::OLD_PASSWORD_PARAM,
            self::PASSWORD_PARAM,
            self::PASSWORDCONFIRM_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::OLD_PASSWORD_PARAM => self::OLD_PASSWORD_RULE,
            self::PASSWORD_PARAM => self::PASSWORD_RULE,
            self::PASSWORDCONFIRM_PARAM => self::PASSWORDCONFIRM_RULE
        ];
    }


}