<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed user_id
 */
class UserIDModel extends RequestParametersModel
{
    protected function parameterKeys(): array
    {
        return [
            self::USER_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::USER_ID_PARAM => self::USER_ID_RULE
        ];
    }
}