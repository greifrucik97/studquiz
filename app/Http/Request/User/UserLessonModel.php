<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed user_id
 * @property string role
 * @property mixed lesson_id
 */
class UserLessonModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::USER_ID_PARAM,
            self::LESSON_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::LESSON_ID_PARAM => self::LESSON_ID_RULE,
            self::USER_ID_PARAM => self::USER_ID_RULE
        ];
    }


}