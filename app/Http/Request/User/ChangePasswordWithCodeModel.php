<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * @property mixed code
 * @property mixed passwordConfirm
 * @property mixed password
 */
class ChangePasswordWithCodeModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::PASSWORD_PARAM,
            self::PASSWORDCONFIRM_PARAM,
            self::CODE_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::PASSWORD_PARAM => self::PASSWORD_RULE,
            self::PASSWORDCONFIRM_PARAM => self::PASSWORDCONFIRM_RULE,
            self::CODE_PARAM => self::CODE_RULE
        ];
    }


}