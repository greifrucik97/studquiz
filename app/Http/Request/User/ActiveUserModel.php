<?php


namespace App\Http\Request\User;


use App\Http\Request\RequestParametersModel;

/**
 * Class CheckLinkModel
 * @property mixed link
 * @package App\Http\Request\User
 */
class ActiveUserModel extends RequestParametersModel
{

    /**
     * Metoda zwracą tablice nazw parametrów, które mają być dostępne w modelu
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::LINK_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::LINK_PARAM => self::LINK_RULE
        ];
    }

    protected function messages(): array
    {
        return [
            'link.string' => 'Pole :attribute powinno być ciągiem znaków',
            'link.required' => 'Pole :attribute jest wymagane',
            'link.exists' => 'Link nie istnieje w bazie'
        ];
    }


}