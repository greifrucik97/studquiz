<?php


namespace App\Http\Request\Tag;


use App\Http\Request\RequestParametersModel;

class GetTagsModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::NAME_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::NAME_PARAM => self::NAME_RULE
        ];
    }

}