<?php


namespace App\Http\Request\Tag;


use App\Http\Request\RequestParametersModel;

class DetachTagModel extends RequestParametersModel
{

    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::QUESTION_ID_PARAM,
            self::TAG_ID_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::QUESTION_ID_PARAM => self::QUESTION_ID_RULE,
            self::TAG_ID_PARAM => self::TAG_ID_RULE
        ];
    }


}