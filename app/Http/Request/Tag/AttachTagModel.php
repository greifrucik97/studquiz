<?php


namespace App\Http\Request\Tag;


use App\Http\Request\RequestParametersModel;

class AttachTagModel extends RequestParametersModel
{


    /**
     * @return array
     */
    protected function parameterKeys(): array
    {
        return [
            self::QUESTION_ID_PARAM,
            self::NAME_PARAM
        ];
    }

    protected function rules(): array
    {
        return [
            self::QUESTION_ID_PARAM => self::QUESTION_ID_RULE,
            self::NAME_PARAM => self::NAME_RULE
        ];
    }


}