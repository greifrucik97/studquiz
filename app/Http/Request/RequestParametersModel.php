<?php
namespace App\Http\Request;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as ValidatorFactory;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use function array_key_exists;


abstract class RequestParametersModel
{
    /**
     * @var array
     */
    private $attributes = [];


    /**
     * @var array
     */
    private $defaults;

    /**
     * @var Validator
     */
    private $validator;


    public const FIRSTNAME_PARAM = 'firstName';
    public const FIRSTNAME_RULE = 'string|required';

    public const CREATE_GROUP_PARAM = 'createGroup';
    public const CREATE_GROUP_RULE = 'boolean|required';

    public const GROUP_NAME_PARAM = 'groupName';
    public const GROUP_NAME_RULE = 'string|nullable';

    public const TYPE2_RULE = 'string|required|in:startDate,endDate';

    public const DATE_PARAM = 'date';
    public const DATE_RULE = 'string|required';

    public const LASTNAME_PARAM = 'lastName';
    public const LASTNAME_RULE = 'string|required';

    public const INDEXNUMBER_PARAM = 'indexNumber';
    public const INDEXNUMBER_RULE = 'string|required';
    public const INDEXNUMBER_SEND_LINK_RULE = 'string|required|unique:account_link_users';

    public const EMAIL_PARAM = 'email';
    public const EMAIL_RULE = 'string|required|unique:users,email';

    public const EMAIL_GROUP_RULE = 'string|required';
    public const EMAIL_REMEMBER_PASSWORD_RULE = 'string|required|exists:users,email';
    public const EMAIL_SEND_LINK_RULE = 'required|string|unique:account_link_users|email:rfc,dns';



    public const OLD_PASSWORD_PARAM = 'oldPassword';
    public const OLD_PASSWORD_RULE = 'required|string|password:api';

    public const PASSWORD_PARAM = 'password';
    public const PASSWORD_RULE = [
        'required',
        'string',
        'min:8',             // must be at least 10 characters in length
        'regex:/[a-z]/',      // must contain at least one lowercase letter
        'regex:/[A-Z]/',      // must contain at least one uppercase letter
        'regex:/[0-9]/',      // must contain at least one digit
        'regex:/[@$!%*#?&]/', // must contain a special character
    ];

    public const PASSWORDCONFIRM_PARAM = 'passwordConfirm';
    public const PASSWORDCONFIRM_RULE = 'string|required|same:password';

    public const CODE_PARAM = 'code';
    public const CODE_RULE = 'string|required|exists:remember_passwords,code';

    public const USER_ID_PARAM = 'user_id';
    public const USER_ID_RULE = 'numeric|required|exists:users,id';

    public const LESSON_ID_PARAM = 'lesson_id';
    public const LESSON_ID_RULE = 'numeric|required|exists:lessons,id';

    public const GROUP_ID_PARAM = 'group_id';
    public const GROUP_ID_RULE = 'numeric|required|exists:groups,id';

    public const ROLE_PARAM = 'role';
    public const ROLE_RULE = 'string|required|in:student,admin,teacher';

    public const LINK_PARAM = 'link';
    public const LINK_RULE = 'string|required|exists:users,activationLink';
    public const ROLE_SEND_LINK_RULE = 'string|required|in:student,admin,teacher';

    public const TITLE_PARAM = 'title';
    public const TITLE_RULE = 'string|required';

    public const CONTENT_PARAM = 'content';
    public const CONTENT_RULE = 'string|required';

    public const OWNER_ID_PARAM = 'owner_id';
    public const OWNER_ID_RULE = 'numeric|nullable|exists:users,id';
    public const OWNER_ID_REQUIRED_RULE = 'numeric|required|exists:users,id';

    public const NAME_PARAM = 'name';
    public const NAME_RULE = 'string|required';

    public const POST_ID_PARAM = 'post_id';
    public const POST_ID_RULE = 'numeric|nullable|exists:posts,id';

    public const COMMENT_ID_PARAM = 'comment_id';
    public const COMMENT_ID_RULE = 'numeric|nullable|exists:comments,id';

    public const TAG_ID_PARAM = 'tag_id';
    public const TAG_ID_RULE = 'numeric|required|exists:tags,id';

    public const LINK_TO_JOIN_PARAM = 'link';
    public const LINK_TO_JOIN_RULE = 'string|required|exists:lessons,invite_code';

    public const QUESTION_PARAM = 'question';
    public const QUESTION_RULE = 'string|required';

    public const TYPE_PARAM = 'type';
    public const TYPE_RULE = 'string|required|exists:question_types,type';

    public const PKT_PARAM = 'pkt';
    public const PKT_RULE = 'numeric|required';

    public const ANSWERS_PARAM = 'answers';
    public const ANSWERS_RULE = 'array';

    public const CORRECT_ANSWER_PARAM = 'correctAnswer';
    public const CORRECT_ANSWER_RULE = 'array';

    public const TEST_ID_PARAM = 'test_id';
    public const TEST_ID_RULE = 'numeric|required|exists:tests,id';

    public const QUESTION_ID_PARAM = 'question_id';
    public const QUESTION_ID_RULE = 'numeric|required|exists:questions,id';

    public const USER_TEST_ID_PARAM = 'user_test_id';
    public const USER_TEST_ID_RULE = 'numeric|required|exists:user_tests,id';

    public const USER_ANSWER_ID_PARAM = 'user_answer_id';
    public const USER_ANSWER_ID_RULE = 'numeric|required|exists:user_answers,id';

    public const TAGS_PARAM = 'tags';
    public const TAGS_RULE = 'array';

    public const USERS_PARAM = 'users';
    public const USERS_RULE = 'required|array';



    /**
     * RequestParametersModel constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->defaults = $this->defaultValues();
        $this->prepareAttributes($request);
        $this->forceDefault();
    }

    /**
     * @return array
     */
    abstract protected function parameterKeys(): array;

    /**
     * @return array
     */
    protected function defaultValues(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function messages(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function forceDefaultRules(): array
    {
        return [];
    }


    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) var_export($this->attributes, true);
    }

    /**
     * @throws ValidationException
     */
    public function validate(): void
    {
        $this->validator = ValidatorFactory::make($this->attributes, $this->getRules(), $this->messages());
        $this->validator->validate();

    }

    /**
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public function __get(string $name)
    {
        if (!array_key_exists($name, $this->attributes)) {
            throw new Exception($name);
        }

        return $this->attributes[$name];
    }

    /**
     * @param string $name
     * @param $value
     */
    public function __set(string $name, $value): void
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name): bool
    {
        if (isset($this->attributes[$name])) {
            return true;
        }

        return false;
    }

    /**
     * @param Request $request
     */
    private function prepareAttributes(Request $request): void
    {
        $keys = $this->parameterKeys();

        foreach ($keys as $key) {
            $this->attributes[$key] = $request->get($key, $this->getDefault($key));
        }
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    private function getDefault(string $key)
    {
        if (array_key_exists($key, $this->defaults)) {
            return $this->defaults[$key];
        }

        return null;
    }

    /**
     *
     */
    private function forceDefault(): void
    {
        $forceRules = $this->forceDefaultRules();

        foreach ($forceRules as $key => $values) {
            if (array_key_exists($key, $this->attributes)) {
                foreach ($values as $value) {
                    if ($value === $this->attributes[$key]) {
                        $this->attributes[$key] = $this->getDefault($key);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    private function getRules(): array
    {
        $rules = $this->rules();

        return $rules;
    }
}
