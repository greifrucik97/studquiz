<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'tests';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = true;

    protected $hidden = ['pivot'];


    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function userTests()
    {
        return $this->hasMany(UserTest::class,'test_id', 'id');
    }
}
