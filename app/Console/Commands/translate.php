<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Stichoza\GoogleTranslate\GoogleTranslate;

class translate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trans {lang=en}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @eturn int
     * @throws \ErrorException
     */
    public function handle()
    {
        $lang = $this->argument('lang');

        if ($lang === "pl") {

            $this->info('Nie możesz tłumaczyć na język Polski');
            return 0;
        }

        $tr = new GoogleTranslate($lang, 'pl', [
            'timeout' => 10,
            'headers' => [
                'User-Agent' => 'Foo/5.0 Lorem Ipsum Browser'
            ],
            'verify' => false
        ]);

        $stringTranslate = "";

        $path = resource_path("js/locales/pl.json");

        $file = file_get_contents($path);
        $decode = json_decode($file);

        $array = [];

        foreach ($decode as $key => $value)
        {
            $stringTranslate = $stringTranslate . $key . "|";
        }

        $stringTranslate = rtrim($stringTranslate, "|");

        if (strlen($stringTranslate) > 5000) {
            $this->info('Masz za duży plik językowy');
            return 0;
        }


        $stringTranslateToLang = $tr->translate($stringTranslate);
        //$stringTranslateToLang = $stringTranslate;

        $arrayToTrans = explode("|", $stringTranslate);
        $arrayToLang = explode("|", $stringTranslateToLang);


        for( $i = 0; $i < count($arrayToTrans); $i++ ) {

            $array[$arrayToTrans[$i]] = trim($arrayToLang[$i]);

        }

        $encode = json_encode($array, JSON_UNESCAPED_UNICODE);

        $pathSave = resource_path("js/locales/" . $lang . ".json");
        file_put_contents($pathSave, $encode);

        $this->info('Zakończono proces:)');
        return 0;
    }
}
