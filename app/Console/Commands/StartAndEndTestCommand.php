<?php

namespace App\Console\Commands;

use App\Http\Services\TestService;
use App\Test;
use App\UserTest;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StartAndEndTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = Carbon::now();
        $date->second = 0;

        $startTests = Test::where('startDate', $date)->get();

        foreach ($startTests as $startTest) {
            $startTest->active = 1;
            $startTest->save();
        }

        $endTests = Test::where('endDate', $date)->get();

        foreach ($endTests as $endTest) {
            $endTest->active = 2;
            $endTest->save();

        }

        return 0;
    }
}
