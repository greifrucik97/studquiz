<?php

namespace App\Console\Commands;

use App\QuestionType;
use Illuminate\Console\Command;

class QuestionTypeInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:questionType';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Question type init';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $questionType = new QuestionType();
        $questionType->type = "singleChoice";
        $questionType->name = "Jednokrotny wybór";

        $questionType1 = new QuestionType();
        $questionType1->type = "multipleChoice";
        $questionType1->name = "Wielokrotny wybór";

        $questionType2 = new QuestionType();
        $questionType2->type = "shortOpenQuestion";
        $questionType2->name = "Krótkie pytanie otwarte";

        $questionType3 = new QuestionType();
        $questionType3->type = "longOpenQuestion";
        $questionType3->name = "Długie pytanie otwarte";



        $questionType->save();
        $questionType1->save();
        $questionType2->save();
        $questionType3->save();


    }
}
