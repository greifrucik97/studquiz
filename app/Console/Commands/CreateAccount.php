<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $user = new User();

        $user->firstName = 'admin';
        $user->lastName = 'admin';
        $user->email = 'admin@admin.pl';
        $user->password = 'admin';
        $user->active = true;

        $user->save();

        return true;
    }
}
