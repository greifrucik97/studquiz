<?php

namespace App\Exceptions;

use App\Http\Exceptions\DontHaveAccessException;
use App\Http\Response\APIResponse;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;
use function var_export;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     * @param Request    $request
     * @param Exception $exception
     * @return JsonResponse
     */
    public function render($request, $exception): JsonResponse
    {

        if ($exception instanceof ValidationException) {
            return $this->renderException(
                $exception,
                422,
                ['error' => $exception->errors()]
            );
        }


        if ($exception instanceof DontHaveAccessException) {
            return $this->renderException(
                $exception,
                403,
                []
            );
        }

        return $this->renderException($exception);
    }

    private function getValidationErrorMessage(ValidationException $exception): string
    {
        $validator = $exception->validator;

        return var_export($validator->errors()->getMessages(), true);
    }

    /**
     * @param $exception
     * @param int $code
     * @param array|null $data
     * @return JsonResponse
     */
    private function renderException(Throwable $exception, int $code = 0, ?array $data = null): JsonResponse
    {
        if (empty($code)) {
            $code = 500;
        }

        return APIResponse::produceResponse($code, $exception->getMessage(), $data);
    }
}
