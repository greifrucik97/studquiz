<?php

namespace App\Exceptions;

use Exception;

class NoActiveAccount extends Exception
{
    protected $message;
    protected $code = 403;

    /**
     * NoActiveAccount constructor.
     */
    public function __construct()
    {
        $this->message = __('messages.Twoje konto nie jest aktywowane. Sprawdź maila oraz aktywuj konto');
    }

}
