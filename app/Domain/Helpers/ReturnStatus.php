<?php


namespace App\Domain\Helpers;

/**
 * Class ReturnStatus
 * @package App\Domain\Helpers
 */
class ReturnStatus
{
    public const UNAUTHORIZED = 401;
    public const BAD_ROLE = 403;
    public const LOCKED = 423;
    public const VALID_ERRORS = 422;
    public const CREATED = 201;
    public const ACCEPTED  = 202;
    public const BAD_REQUEST = 400;
    public const NOT_FOUND = 404;
}