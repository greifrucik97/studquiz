<?php


namespace App\Domain\Helpers;

/**
 * Class UserHelper
 * @package App\Helpers
 */
class UserHelper
{
    public static function getUserIdFromToken() {

        return auth()->user()->id;

    }

    public static function getUserFromToken() {

        return auth()->user();

    }
}