<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed answers
 * @property mixed goodAnswers
 * @property  int owner_id
 * @property int type_id
 * @property string question
 * @property Double pkt
 */
class Question extends Model
{
    protected $table = 'questions';

    protected $primaryKey = 'id';

    protected $hidden = ['pivot'];

    public $incrementing = true;

    public $timestamps = true;

    protected $fillable = ['question', 'type_id', 'answers'];

    protected $casts = [
        'answers' => 'array'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function type()
    {
        return $this->belongsTo(QuestionType::class, 'type_id');
    }

    public function tests()
    {
        return $this->belongsToMany(Test::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
