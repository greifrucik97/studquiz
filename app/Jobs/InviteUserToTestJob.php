<?php

namespace App\Jobs;

use App\Mail\InviteUserToTestMail;
use App\Test;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class InviteUserToTestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Test
     */
    private $test;
    /**
     * @var User
     */
    private $user;


    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Test $test
     */
    public function __construct(User $user, Test $test)
    {
        $this->user = $user;
        $this->test = $test;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user)->send(new InviteUserToTestMail($this->test));
    }
}
