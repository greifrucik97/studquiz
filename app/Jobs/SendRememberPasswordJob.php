<?php

namespace App\Jobs;

use App\Mail\RegisterMail;
use App\Mail\SendRememberPasswordMail;
use App\RememberPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendRememberPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var RememberPassword
     */
    public $rememberPassword;

    /**
     * SendRememberPasswordJob constructor.
     * @param RememberPassword $rememberPassword
     */
    public function __construct(RememberPassword $rememberPassword)
    {
        $this->rememberPassword = $rememberPassword;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->rememberPassword->email)->send(new SendRememberPasswordMail($this->rememberPassword->code));
    }
}
