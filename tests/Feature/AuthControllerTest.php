<?php

namespace Tests\Feature;

use App\Mail\SendRememberPasswordMail;
use App\RememberPassword;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Psy\Util\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_create()
    {
        $formData = [
            "email" => "test@test.pl",
            "firstName" => "Gracjan",
            "lastName" => "Młynarczyk",
            "password" => "zaq1@WSX",
            "passwordConfirm" => "zaq1@WSX"
        ];

        $this->post('/api/auth/createUserAccount', $formData)->assertStatus(200);
    }

    public function test_can_active_account(){

        $user = factory(User::class)->create([
            'active' => 0,
            'activationLink' => \Illuminate\Support\Str::random(10)
        ]);
        $user->save();
        $this->post('/api/auth/activeUser', [
            "link" => $user->activationLink
        ])->assertStatus(200);

        $user = User::where('email', $user->email)->first();

        $xd = $user->active;
        self::assertTrue($user->active == '1');
    }


    public function test_can_login_user()
    {
        $user = factory(User::class)->create();

        $user->save();
        $response = $this->post('/api/auth/login', [
            'email' => $user->email,
            'password' => "password"
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token', 'token_type', 'expires_in'
            ]);
    }

    public function test_can_logout(){
        $user = factory(User::class)->create();

        $this->actingAs($user)->post('/api/auth/logout')->assertStatus(200);
    }

    public function test_get_me() {
        $user = factory(User::class)->create();

        $this->actingAs($user)->post('/api/auth/me')->assertStatus(200)
        ->assertJson([
            'firstName' => $user->firstName,
            'lastName' => $user->lastName,
            'email' => $user->email
        ]);
    }

    public function test_user_can_send_remember_password_email(){

        $user = factory(User::class)->create();

        $this->post('/api/auth/rememberPassword', [
            'email' => $user->email
        ])->assertStatus(200);
    }

    public function test_change_password_with_code() {

        $user = factory(User::class)->create();

        $rememberPassword = factory(RememberPassword::class)->create([
            'email' => $user->email,
            'user_id' => $user->id
        ]);

        $this->post('/api/auth/changePasswordWithCode', [
            'code' => $rememberPassword->code,
            'password' => "zaq1@WSX",
            'passwordConfirm' => "zaq1@WSX"
        ])->assertStatus(200);

        $response = $this->post('/api/auth/login', [
            'email' => $user->email,
            'password' => "zaq1@WSX"
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token', 'token_type', 'expires_in'
            ]);

    }

    public function test_can_refresh_token(){
        $user = factory(User::class)->create();

        $this->actingAs($user)->post("/api/auth/refresh")->assertStatus(200);
    }

    public function test_user_can_change_password(){

        $user = factory(User::class)->create();

        $this->actingAs($user)->post("/api/auth/changePassword", [
            'oldPassword' => 'password',
            'password' => 'zaq1@WSX',
            'passwordConfirm' => 'zaq1@WSX'
        ])->assertStatus(200);

        $response = $this->post('/api/auth/login', [
            'email' => $user->email,
            'password' => "zaq1@WSX"
        ])->assertStatus(200);
    }

    public function test_delete_user() {
        $user = factory(User::class)->create();

        $this->actingAs($user)->post('/api/user/deleteUser', [
            'user_id' => $user->id
        ])->assertStatus(200);

    }




}
