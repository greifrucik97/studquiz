<?php

namespace Tests\Feature;

use App\Question;
use App\Test;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Faker\Generator as Faker;

class TestControllerTest extends TestCase
{

    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        Artisan::call('command:init');
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_create_test()
    {
        $this->actingAs($this->user)->post('/api/test/createTest', [
            'name' => 'name'
        ])->assertStatus(200);
    }

    public function test_can_get_tests() {

        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);
        $test1 = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/getTests')->assertStatus(200);

    }

    public function test_can_delete_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/deleteTest',[
            'test_id' => $test->id
        ])->assertStatus(200);
    }

    public function test_can_edit_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/editTest',[
            'test_id' => $test->id,
            'name' => 'newName'
        ])->assertStatus(200);
    }

    public function test_can_attach_question_to_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $question = factory(Question::class)->create([
            'owner_id' => $this->user->id,
            'type_id' => 1
        ]);

        $this->actingAs($this->user)->post('/api/test/attachQuestion',[
            'test_id' => $test->id,
            'question_id' => $question->id
        ])->assertStatus(200);
    }

    public function test_can_detach_question_to_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $question = factory(Question::class)->create([
            'owner_id' => $this->user->id,
            'type_id' => 1
        ]);

        $this->actingAs($this->user)->post('/api/test/attachQuestion',[
            'test_id' => $test->id,
            'question_id' => $question->id
        ])->assertStatus(200);

        $this->actingAs($this->user)->post('/api/test/detachQuestion',[
            'test_id' => $test->id,
            'question_id' => $question->id
        ])->assertStatus(200);
    }

    public function test_can_get_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);
        $this->actingAs($this->user)->post('/api/test/getTest',[
            'test_id' => $test->id,
        ])->assertStatus(200);
    }

    public function test_can_active_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/activeTest',[
            'test_id' => $test->id,
        ])->assertStatus(200);
    }


    public function test_can_un_active_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/unActiveTest',[
            'test_id' => $test->id,
        ])->assertStatus(200);
    }

    public function test_can_start_test() {
        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/startTest', [
            'test_id' => $test->id
        ])->assertStatus(200);
    }
//    public function test_can_finish_test() {
//        $test = factory(Test::class)->create([
//            'owner_id' => $this->user->id
//        ]);
//
//        //todo
//        $this->actingAs($this->user)->post('/api/test/finishTest', [
//            'test_id' => $test->id,
//            'answers' => []
//        ])->assertStatus(200);
//    }

    public function test_attach_user_to_test() {

        $users = [
            'email1@test.pl',
            $this->user->email
        ];


        $test = factory(Test::class)->create([
            'owner_id' => $this->user->id
        ]);

        $this->actingAs($this->user)->post('/api/test/attachUsersToTest', [
            'users' => $users,
            'test_id' => $test->id,
            'groupName' => 'newGroup',
            'createGroup' => true
        ])->assertStatus(200);



    }

}
