<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class QuestionTypesControllerTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_get_question_types()
    {
        Artisan::call('command:init');
        $user = factory(User::class)->create();

        $this->actingAs($user)->post('/api/question/getTypes')->assertStatus(200);
    }
}
