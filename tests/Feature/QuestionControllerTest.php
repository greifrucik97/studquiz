<?php

namespace Tests\Feature;

use App\Image;
use App\Question;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class QuestionControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_create_question_single_choice()
    {
        Artisan::call('command:init');
        $user = factory(User::class)->create();
        $image = factory(Image::class)->create();
        $image1 = factory(Image::class)->create();
        $image2 = factory(Image::class)->create();
        $image3 = factory(Image::class)->create();

        $formData = [
            'question' => 'tresc pytania',
            'type' => 'singleChoice',
            'pkt' => 1.0,
            'answers' => [
                ['id' => 1, 'name' => "jeden"],
                ['id' => 2, 'name' => "dwa"],
                ['id' => 3, 'name' => "trzy"],
                ['id' => 4, 'name' => "cztery"],
            ],
            'correctAnswer' => [2],
            'tags' => ['php', 'programowanie'],
            'assignImages' => [
                ['value' => 1, 'name' => $image->name],
                ['value' => 2, 'name' => $image1->name],
                ['value' => 3, 'name' => $image2->name],
                ['value' => 4, 'name' => $image3->name],
            ],
            'imagesReturn' => [$image, $image1, $image2, $image3]
        ];

        $this->actingAs($user)->post('/api/question/createQuestion', $formData)->assertStatus(200);
    }

    public function test_can_create_question_multiple_choice() {

        Artisan::call('command:init');
        $user = factory(User::class)->create();
        $image = factory(Image::class)->create();
        $image1 = factory(Image::class)->create();
        $image2 = factory(Image::class)->create();
        $image3 = factory(Image::class)->create();

        $formData = [
            'question' => 'tresc pytania',
            'type' => 'multipleChoice',
            'pkt' => 3.0,
            'answers' => [
                ['id' => 1, 'name' => "jeden"],
                ['id' => 2, 'name' => "dwa"],
                ['id' => 3, 'name' => "trzy"],
                ['id' => 4, 'name' => "cztery"],
            ],
            'correctAnswer' => [2, 1, 3],
            'tags' => [],
            'assignImages' => [
                ['value' => 1, 'name' => $image->name],
                ['value' => 2, 'name' => $image1->name],
                ['value' => 3, 'name' => $image2->name],
                ['value' => 4, 'name' => $image3->name],
            ],
            'imagesReturn' => [$image, $image1, $image2, $image3]
        ];

        $this->actingAs($user)->post('/api/question/createQuestion', $formData)->assertStatus(200);

    }

    public function test_can_create_question_short_open() {

        Artisan::call('command:init');
        $user = factory(User::class)->create();
        $image = factory(Image::class)->create();

        $formData = [
            'question' => 'tresc pytania',
            'type' => 'shortOpenQuestion',
            'pkt' => 3.0,
            'answers' => [],
            'correctAnswer' => ['odpowiedz na pytanie'],
            'tags' => [],
            'assignImages' => [
                ['value' => 'title', 'name' => $image->name],
            ],
            'imagesReturn' => [$image]
        ];

        $this->actingAs($user)->post('/api/question/createQuestion', $formData)->assertStatus(200);
    }

    public function test_can_create_question_long_open() {
        Artisan::call('command:init');
        $user = factory(User::class)->create();

        $formData = [
            'question' => 'tresc pytania',
            'type' => 'longOpenQuestion',
            'pkt' => 3.0,
            'answers' => [],
            'correctAnswer' => ['odpowiedz na pytanie'],
            'tags' => [],
            'assignImages' => [],
            'imagesReturn' => []
        ];

        $this->actingAs($user)->post('/api/question/createQuestion', $formData)->assertStatus(200);
    }

    public function test_can_get_questions() {
        Artisan::call('command:init');
        $user = factory(User::class)->create();

        $question = factory(Question::class)->create([
            'type_id' => 1,
            'owner_id' => $user->id
        ]);

        $question1 = factory(Question::class)->create([
            'type_id' => 2,
            'owner_id' => $user->id
        ]);

        $question2 = factory(Question::class)->create([
            'type_id' => 1,
            'owner_id' => $user->id
        ]);

        $question3 = factory(Question::class)->create([
            'type_id' => 2,
            'owner_id' => $user->id
        ]);

        $this->actingAs($user)->post('/api/question/getQuestions')->assertStatus(200);

        $this->actingAs($user)->post('/api/question/getQuestions', [
            'q' => $question->name,
            'filter' => [
                'type_id' => [1]
            ]
        ])->assertStatus(200);

    }

    public function test_can_get_question() {
        Artisan::call('command:init');
        $user = factory(User::class)->create();

        $question = factory(Question::class)->create([
            'type_id' => 1,
            'owner_id' => $user->id
        ]);

        $this->actingAs($user)->post('/api/question/getQuestion', [
            'question_id' => $question->id
        ])->assertStatus(200);
    }

    public function test_can_delete_question() {
        Artisan::call('command:init');
        $user = factory(User::class)->create();
        $user1 = factory(User::class)->create();

        $question = factory(Question::class)->create([
            'type_id' => 1,
            'owner_id' => $user->id
        ]);

        $this->actingAs($user1)->post('/api/question/deleteQuestion', [
            'question_id' => $question->id
        ])->assertStatus(200);

        $this->actingAs($user)->post('/api/question/deleteQuestion', [
            'question_id' => $question->id
        ])->assertStatus(200);
    }

    public function test_can_edit_question() {
        Artisan::call('command:init');
        $user = factory(User::class)->create();

        $question = factory(Question::class)->create([
            'type_id' => 2,
            'owner_id' => $user->id,
        ]);


        $this->actingAs($user)->post('/api/question/editQuestion', [
            'question_id' => $question->id,
            'question' => 'nowa tresc pytania',
            'pkt' => $question->pkt,
            'answers' => [
                ['id' => 1, 'name' => "jeden"],
                ['id' => 2, 'name' => "dwa"],
                ['id' => 3, 'name' => "trzy"],
                ['id' => 4, 'name' => "cztery"],
            ],
            'correctAnswer' => [2, 1, 3],
            'tags' => [],
            'assignImages' => [],
            'imagesReturn' => []
        ])->assertStatus(200);

    }

}
