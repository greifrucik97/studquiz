<?php

namespace Tests\Feature;

use App\Group;
use App\GroupEmail;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GroupControllerTest extends TestCase
{

    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function test_can_get_user_groups()
    {
        $group = factory(Group::class)->create(['owner_id' => $this->user->id]);
        $group2 = factory(Group::class)->create(['owner_id' => $this->user->id]);
        $group3 = factory(Group::class)->create(['owner_id' => $this->user->id]);
        $group4 = factory(Group::class)->create(['owner_id' => $this->user->id]);

        $this->actingAs($this->user)->post('/api/group/getUserGroups')->assertStatus(200);
    }

    public function test_can_get_group_users() {

        $group = factory(Group::class)->create(['owner_id' => $this->user->id]);
        $groupEmail = factory(GroupEmail::class)->create(['group_id' => $group->id]);
        $groupEmail1 = factory(GroupEmail::class)->create(['group_id' => $group->id]);
        $groupEmail2 = factory(GroupEmail::class)->create(['group_id' => $group->id]);
        $groupEmail3 = factory(GroupEmail::class)->create(['group_id' => $group->id]);

        $this->actingAs($this->user)->post('/api/group/getGroupUsers', [
            'group_id' => $group->id
        ])->assertStatus(200);

    }

    public function test_can_delete_group() {
        $group = factory(Group::class)->create(['owner_id' => $this->user->id]);
        $groupEmail = factory(GroupEmail::class)->create(['group_id' => $group->id]);

        $this->actingAs($this->user)->post('/api/group/deleteGroup', [
            'group_id' => $group->id
        ])->assertStatus(200);
    }

    public function test_add_user_to_group() {
        $group = factory(Group::class)->create(['owner_id' => $this->user->id]);

        $this->actingAs($this->user)->post('/api/group/addUserToGroup', [
            'group_id' => $group->id,
            'email' => 'test@test.pl'
        ])->assertStatus(200);
    }

    public function test_remove_user_to_group() {
        $group = factory(Group::class)->create(['owner_id' => $this->user->id]);
        $groupEmail = factory(GroupEmail::class)->create(['group_id' => $group->id]);

        $this->actingAs($this->user)->post('/api/group/removeUserToGroup', [
            'group_id' => $group->id,
            'email' => $groupEmail->email
        ])->assertStatus(200);
    }
}
