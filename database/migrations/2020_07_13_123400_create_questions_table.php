<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id('id');
            $table->string('question');
            $table->unsignedBigInteger('type_id');
            $table->json('answers')->nullable();
            $table->float('pkt');
            $table->unsignedBigInteger('owner_id');
            $table->string('image_path')->nullable();
            $table->timestamps();
            $table->foreign('type_id')->references('id')->on('question_types');
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
