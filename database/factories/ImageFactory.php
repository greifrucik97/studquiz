<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Image::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'path' => $faker->name . '.' . $faker->fileExtension
    ];
});
