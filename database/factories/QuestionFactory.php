<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(\App\Question::class, function (Faker $faker) {
    return [
        'question' => $faker->name,
        'pkt' => $faker->randomFloat(null, 1, 5),
    ];
});
