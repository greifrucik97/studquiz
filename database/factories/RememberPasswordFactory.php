<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\RememberPassword::class, function (Faker $faker) {
    return [
        "code" => Str::random(10)
    ];
});
