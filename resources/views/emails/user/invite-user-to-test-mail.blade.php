@component('mail::message')
    {{-- use double space for line break --}}
    Zostałeś zaproszony do rozwiązania testu użytkownika:
    {{  $test->owner->firstName . " " . $test->owner->lastName  }}

    Nazwa testu
    {{ $test->name  }}

    Wejdź na stronę oraz zaakceptuj rozwiązanie testu.

{{--@component('mail::button', ['url' => $url . '/user/active/' . $user->activationLink])--}}
{{--    Aktywacja--}}
{{--@endcomponent--}}

    Dziękujemy, <br>
    StudQuiz
@endcomponent
