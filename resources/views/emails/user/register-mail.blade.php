@component('mail::message')
    {{-- use double space for line break --}}
    Aktywuj swoje konto <br>
    Aby zakończyć rejestracje aktywuj konto

    {{env("APP_VUE_URL") }}

@component('mail::button', ['url' => $url . '/user/active/' . $user->activationLink])
    Aktywacja
@endcomponent

    Jeżeli przycisk nie działa skopiuj link oraz wprowadź go w oknie przegladarki <br>
    <a href="{{ $url . '/user/active/' . $user->activationLink }}">{{ $url . '/user/active/' . $user->activationLink }}</a>

    Dziękujemy,
    StudQuiz
@endcomponent
