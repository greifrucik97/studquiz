require('./bootstrap');
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Notifications from 'vue-notification'
import jwtDecode from 'jwt-decode'
import { get } from 'lodash'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import SmartTable from 'vuejs-smart-table'
import Vuelidate from 'vuelidate'
import VueRouter from 'vue-router'
import {EventBus} from "./event-bus";
import $ from 'jquery'


Vue.config.productionTip = false


Vue.use(VueAxios, axios);
Vue.use(Notifications);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(SmartTable);
Vue.use(Vuelidate);
Vue.use(VueRouter);

import JwPagination from 'jw-vue-pagination';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import * as types from './store/mutation-types'
import i18n from './i18n'

Vue.component('jw-pagination', JwPagination);
import Paginate from 'vuejs-paginate'
Vue.component('paginate', Paginate)


Vue.axios.interceptors.request.use(config => {
    if(localStorage.getItem('token')) {
        config.headers['Authorization'] = localStorage.getItem('token')
    }
    if (localStorage.getItem('lang')) {
        if (config.hasOwnProperty('data')) {
            config.data.lang = localStorage.getItem('lang');
        }
    }
    config.headers['Content-Type'] = 'application/json';
    return config;
});

Vue.axios.interceptors.response.use(response => {
    const newtoken = get(response, 'headers.authorization')
    if (newtoken) {
        localStorage.setItem('token', newtoken)
    }
    return response
});

router.beforeEach((to, from, next) => {
    if (localStorage.getItem('lang')) {
        i18n.locale = localStorage.getItem('lang')
    }
    if (localStorage.getItem('token')) {
        let token = localStorage.getItem('token');
        let decodeToken = jwtDecode(token);
        store.dispatch(types.SETID, decodeToken.user.id)
        store.dispatch(types.LOGIN)
    }
    next();
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
    i18n
});