import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Register from "../views/Register";
import User from "../views/User";
import Panel from "../views/Panel";
import Settings from "../views/Settings";
import SettingsAccountComponent from "../components/Settings/SettingsAccountComponent";
import SettingsChangePasswordComponent from "../components/Settings/SettingsChangePasswordComponent";
import RememberPassword from "../views/RememberPassword";
import PanelAddQuestionComponent from "../components/Panel/PanelAddQuestionComponent";
import PanelQuestionsComponent from "../components/Panel/PanelQuestionsComponent";
import Tests from "../views/Tests";
import Test from "../views/Test";
import Question from "../views/Question";
import Quiz from "../views/Quiz";
import UserTest from "../views/UserTest";
import ActiveUser from "../views/ActiveUser";
import Groups from "../views/Groups";



  const routes = [
    {
      path: '/',
      name: 'hello',
      component: Home
    },
    {
      path: '/register',
      name: 'registerUser',
      component: Register
    },
    {
      path: '/user/active/:link',
      name: 'ActiveUser',
      component: ActiveUser
    },
    {
      path: '/user',
      name: 'user',
      component: User
    },
    {
      path: '/remember',
      name: 'remember',
      component: RememberPassword
    },
    {
      path: '/test/:tid',
      name: 'Quiz',
      component: Quiz
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings,
      children: [
        {
          path: '/settings/account',
          component: SettingsAccountComponent,
          name: 'SettingsAccountComponent'
        },
        {
          path: '/settings/changePassword',
          component: SettingsChangePasswordComponent,
          name: 'SettingsChangePasswordComponent'
        },
      ]
    },
    {
      path: '/panel',
      name: 'admin',
      component: Panel,
      children: [
        {
          path: '/panel/test/user/:id',
          component: UserTest,
          name: 'UserTest'
        },
        {
          path: '/panel/questions/add',
          component: PanelAddQuestionComponent,
          name: 'PanelAddQuestionComponent'
        },
        {
          path: '/panel/questions',
          component: PanelQuestionsComponent,
          name: 'PanelQuestionsComponent'
        },
        {
          path: '/panel/question/:id',
          component: Question,
          name: 'Question'
        },
        {
          path: '/panel/tests',
          component: Tests,
          name: 'Tests'
        },
        {
          path: '/panel/groups',
          component: Groups,
          name: 'Groups'
        },
        {
          path: '/panel/test/:id',
          component: Test,
          name: 'Test'
        },
      ]
    },
    {
      path: '*',
      component: Home
    }

  ];

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
