export default {
    logged(state) {
        return state.logged
    },
    role(state) {
        return state.role
    },
    isTeacher(state) {
        return state.role === "teacher" || state.role === "admin"
    },
    isAdmin(state) {
        return state.role === "admin"
    },
    isStudent(state) {
        return state.role === "student" || state.role === "teacher" || state.role === "admin"
    },
    userID(state) {
        return state.userID
    }
}