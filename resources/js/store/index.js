import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import mutations from './mutations';
import actions from './actions';
import getters from "./getters";


export default new Vuex.Store({
  state: {
    logged: 0,
    role: "",
    userID: null
  },
  mutations,
  actions,
  getters
})
