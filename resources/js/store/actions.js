import * as types from './mutation-types'

export default {
    [types.LOGIN]({ commit }) {
        commit(types.LOGIN)
    },
    [types.LOGOUT]({ commit }) {
        commit(types.LOGOUT)
    },
    [types.ADMIN]({ commit }) {
        commit(types.ADMIN)
    },
    [types.TEACHER]({ commit }) {
        commit(types.TEACHER)
    },
    [types.STUDENT]({ commit }) {
        commit(types.STUDENT)
    },
    [types.CLEAR]({ commit }) {
        commit(types.CLEAR)
    },
    [types.SETID]({ commit }, payload) {
        commit(types.SETID, payload)
    },
}