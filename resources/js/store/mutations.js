import * as types from './mutation-types'

export default {
    [types.LOGIN]( state ) {
        state.logged = 1;
    },
    [types.LOGOUT]( state ) {
        state.logged = 0;
    },
    [types.ADMIN]( state ) {
        state.role = "admin"
    },
    [types.TEACHER]( state ) {
        state.role = "teacher"
    },
    [types.STUDENT]( state ) {
        state.role = "student"
    },
    [types.CLEAR]( state ) {
        state.role = ""
    },
    [types.SETID]( state, payload ) {
        state.userID = payload
    }
}