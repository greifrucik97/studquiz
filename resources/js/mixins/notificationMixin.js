import Vue from 'vue'



export default {
    methods: {
        notifySuccess: function (title, text) {
            Vue.notify({
                group: 'foo',
                type: "success",
                title: title,
                text: text,
                duration: -1
            })
        },
        notifyWarn: function (title, text) {
            Vue.notify({
                group: 'foo',
                type: "warn",
                title: title,
                text: text,
                duration: -1
            })
        },
        notifyError: function (title, text) {
            Vue.notify({
                group: 'foo',
                type: "error",
                title: title,
                text: text,
                duration: -1
            })
        },
    }
}